#include <errno.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>

#include <iostream>
#include <string>
#include <map>
#include <deque>
#include <algorithm>
#include <fstream>

struct file_info_t {
  std::string           name;
  std::map<int, size_t> events;

  std::string show() const {
    static const std::map<size_t, std::string> names = {
      { IN_ACCESS,        "IN_ACCESS" },
      { IN_MODIFY,        "IN_MODIFY" },
      { IN_ATTRIB,        "IN_ATTRIB" },
      { IN_CLOSE_WRITE,   "IN_CLOSE_WRITE" },
      { IN_CLOSE_NOWRITE, "IN_CLOSE_NOWRITE" },
      { IN_OPEN,          "IN_OPEN" },
      { IN_MOVED_FROM,    "IN_MOVED_FROM" },
      { IN_MOVED_TO,      "IN_MOVED_TO" },
      { IN_CREATE,        "IN_CREATE" },
      { IN_DELETE,        "IN_DELETE" },
      { IN_DELETE_SELF,   "IN_DELETE_SELF" },
      { IN_MOVE_SELF,     "IN_MOVE_SELF" },
      { IN_UNMOUNT,       "IN_UNMOUNT" },
      { IN_Q_OVERFLOW,    "IN_Q_OVERFLOW" },
      { IN_IGNORED,       "IN_IGNORED" },
      { IN_ONLYDIR,       "IN_ONLYDIR" },
      { IN_DONT_FOLLOW,   "IN_DONT_FOLLOW" },
      { IN_EXCL_UNLINK,   "IN_EXCL_UNLINK" },
      { IN_MASK_ADD,      "IN_MASK_ADD" },
      { IN_ISDIR,         "IN_ISDIR" },
      { IN_ONESHOT,       "IN_ONESHOT" },
    };

    std::string str;
    str += name;
    str += "\t\t";
    for (const auto& kv : events) {
      if (!kv.second)
        continue;
      auto it = names.find(kv.first);
      str += (it != names.end() ? it->second : std::string("(unknown)")) + ": " + std::to_string(kv.second) + "\t\t";
    }
    return str;
  }
};

struct monitor_t {
  using wfds_t = std::map<int/*fd*/, int/*file_id*/>;
  using files_t = std::deque<file_info_t>;
  using errors_t = std::deque<std::string>;

  int fd;
  nfds_t nfds;
  struct pollfd fds[2];

  wfds_t   wfds;
  files_t  files;
  errors_t errors;

  monitor_t(const std::string& dir) {
    fd = inotify_init1(IN_NONBLOCK);
    if (fd == -1) {
      throw std::runtime_error("inotify_init1");
    }

    files_scan(dir);
    for (size_t i{}; i < files.size(); ++i) {
      add_watch(i);
    }

    nfds = 2;
    fds[0].fd = STDIN_FILENO;
    fds[0].events = POLLIN;
    fds[1].fd = fd;
    fds[1].events = POLLIN;
  }

  ~monitor_t() {
    close(fd);
  }

  bool process() {
    int poll_num = poll(fds, nfds, -1);
    if (poll_num == -1) {
      if (errno == EINTR)
        return true;
      errors.push_back("poll");
      return true;
    }

    if (!poll_num) {
      return true;
    }

    if (fds[0].revents & POLLIN) {
      return check_cmd();
    }

    if (fds[1].revents & POLLIN) {
      process_events();
    }

    return true;
  }

  void add_watch(size_t ind) {
    if (ind >= files.size())
      return;

    int wd = inotify_add_watch(fd, files[ind].name.c_str(), IN_ALL_EVENTS);

    if (wd == -1) {
      errors.push_back("inotify_add_watch: " + files[ind].name + " " + std::to_string(ind) + "/" + std::to_string(files.size()));
      return;
    }

    wfds[wd] = ind;
  }

  bool check_cmd() const {
    char buf;
    std::string cmd;

    while (read(STDIN_FILENO, &buf, sizeof(buf)) > 0 && buf != '\n') {
      cmd += buf;
    }

    if (cmd == "exit") {
      return false;
    } else if (cmd == "" || cmd == "show") {
      std::cout << show() << std::endl;
    } else if (cmd == "save") {
      std::ofstream file("output");
      if (file.is_open()) {
        file << show();
        file.close();
      }
    } else {
      std::cerr << "unknown cmd" << std::endl;
    }

    return true;
  }

  void process_events() {
    char buf[4096];
    const struct inotify_event *event;
    char *ptr;

    for (;;) {
      int len = read(fd, buf, sizeof(buf));
      if (len == -1 && errno != EAGAIN) {
        errors.push_back("read");
        return;
      }

      if (len <= 0)
        break;

      for (ptr = buf; ptr < buf + len; ptr += sizeof(struct inotify_event) + event->len) {
        event = (const struct inotify_event *) ptr;

        if (event->wd == -1) {
          errors.push_back("event: " + std::string(event->name, event->len) + " " + std::to_string(event->mask));
          continue;
        }

        file_info_t& file = files.at(wfds.at(event->wd)); // Всегда должны попадать
        for (int id{}; id < 8 * sizeof(event->mask); ++id) {
          file.events[1 << id] += (event->mask & (1 << id)) ? 1 : 0;
        }

        if (event->mask & IN_CREATE) {
          std::string name = file.name + "/" + event->name;
          if (event->mask & IN_ISDIR) {
            files_scan(name);
          } else {
            files.push_back({.name = name});
          }
          add_watch(files.size() - 1);
        }
      }
    }
  }

  void files_scan(const std::string& dir_base) {
    DIR* dir = opendir(dir_base.c_str());

    if (dir == NULL) {
      errors.push_back("opendir: " + dir_base);
      return;
    }

    struct dirent *ent;
    while ((ent = readdir (dir)) != NULL) {
      if (strcmp(ent->d_name, "..") == 0) {
        continue;
      }

      std::string name = dir_base;
      if (strcmp(ent->d_name, ".") != 0) {
        name += "/" + std::string(ent->d_name);
      }

      auto it = std::find_if(files.begin(), files.end(), [&name](const file_info_t& file){ return file.name == name; });
      if (it != files.end()) {
        continue;
      }

      files.push_back({.name = name});
      if (ent->d_type == DT_DIR) {
        files_scan(name);
      }
    }
    closedir (dir);
  }

  std::string show() const {
    std::string str;
    str += "==========";
    str += "\n";
    for (const auto& file : files) {
      str += file.show();
      str += "\n";
    }
    str += "errors:\n";
    for (const auto& error : errors) {
      str += error;
      str += "\n";
    }
    return str;
  }
};



int main(int argc, char* argv[]) {

  if (argc != 2) {
    printf("Usage: %s PATH \n", argv[0]);
    exit(EXIT_FAILURE);
  }

  bool is_success = true;

  try {
    monitor_t monitor(argv[1]);
    std::cout << "starting ..." << std::endl;
    while (monitor.process()) {
      // std::cout << monitor.show() << std::endl;
    }
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    is_success = false;
  }

  return !is_success;
}
